import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons'
// Toats
import { toast } from 'react-toastify';

const Burger = ({burger, cart, setCart, burgers}) =>  {
    
    const {img, name, details, price , id} = burger
    const addBurger = id => {
        const burger = burgers.filter((burger) => burger.id === id)
        setCart([...cart, ...burger])
        toast.success('Your Burger saved successfully in your purchase 🍔', {
            position: "bottom-right",
            autoClose: 5000,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        });
    }
    
    return (
        <div className='container__list'>
            <div className='card'>
                <img src={img} alt='food pictures for burgers'/>
                <div className='content-card'>
                    <p className='title'>{name}</p>
                    <p className='details'>{details}</p>

                    <div className='actions__buttons'>
                        <button className='price__btn' disabled>
                            $ <strong>{price}</strong>
                        </button>

                        <button className='Select__btn' onClick={() => addBurger(id)}>
                            <FontAwesomeIcon icon={faShoppingCart}></FontAwesomeIcon> Select
                        </button>      
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Burger;