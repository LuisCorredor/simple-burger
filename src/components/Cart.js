import React from 'react';
import CartBurgerDetail from './CartBurgerDetail';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHamburger, faShoppingBag, faTimesCircle, faShoppingCart } from '@fortawesome/free-solid-svg-icons'
import './cart.css'

// Toats
import { toast } from 'react-toastify';

const Cart = ({cart, setCart}) => {

    const list = cart.map(item => ({
        price: item.price
    }));
    const TotalCost = list.reduce((prev, next) => prev + next.price, 0);

    const viewCart = () => {
        {document.querySelector('div.overlay').style.display = 'none' ? (document.querySelector('div.overlay').style.display = 'flex') : (document.querySelector('div.overlay').style.display = 'none');}
    }

    const closeCart = () => {
        document.querySelector('div.overlay').style.display = 'none';
    }

    const finalizeOrder = () => {
        cart = []
        setCart([...cart])
        closeCart()
        toast.success('Your order is being prepared, enjoy it 🍔', {
            position: "bottom-right",
            autoClose: 5000,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        }); 
    }
    
    return(
        <div>
            <div className='cart__layout'>
                <button onClick={() => viewCart()} disabled={cart.length === 0}>
                    <FontAwesomeIcon icon={faShoppingBag}></FontAwesomeIcon> 
                    View order 
                    <span className='totalPrice'>
                        $ {cart.length === 0 ? (<strong> {cart.length} </strong>) : (<strong> {TotalCost} </strong>)}
                    </span>
                </button>
            </div>
        
            <div className='overlay'>
                    <div className='scroll__prop'>
                        <div className='modal__cart'>
                            <FontAwesomeIcon onClick={() => closeCart()} className='out-icon' icon={faTimesCircle}></FontAwesomeIcon>
                            <div className='card__details'>
                                {cart.length === 0 ? (
                                    <div className='empty-message'>
                                        <FontAwesomeIcon className='alert-icon' icon={faHamburger}></FontAwesomeIcon>
                                        <h1>UPS!</h1>
                                        <p>Your order is empty</p>
                                        <button type='button' onClick={() => closeCart()}>Back to page</button>
                                    </div>  
                                ) : (
                                    cart.map(burger => 
                                    <CartBurgerDetail key ={burger.id} burger={burger} cart ={cart} setCart={setCart}/>
                                ))}
                            </div>
                        </div>
                    </div>

                {cart.length === 0 ?(
                    ''
                ) : (
                    <div className='get__order'>
                        <button type='button' className='btn-finalize' onClick={() => finalizeOrder()}>
                            <FontAwesomeIcon icon={faShoppingCart}></FontAwesomeIcon> 
                                Finalize order
                            <span className='totalPrice'>
                                $ {cart.length === 0 ? (<strong> {cart.length} </strong>) : (<strong> {TotalCost} </strong>)}
                            </span>
                        </button>
                    </div>
                )}
            </div>
        </div>
    );
};

export default Cart;