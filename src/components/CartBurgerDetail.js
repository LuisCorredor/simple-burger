import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons'

const Burger = ({burger, cart, setCart}) =>  {
    
    const {img, name, details, price, toppins, toppingType, id} = burger

    const isToppinsOptions = toppins;
    const istoppinsType = toppingType;
    const combo = 'combo';
    const burgerTopping = 'burger';
    const soda = 'soda';

    const delBurger = (id) => {
        const burgers = cart.filter(burger => burger.id !== id)
        setCart(burgers)
    }

    return (
        <div className='detail__cart_order'>
            <div className="header--order">
                <img src={img} alt='food pictures for burgers'/>
                <div className='content-card'>
                    <p className='title'>{name}</p>
                    <p className='details'>{details}</p>
                </div>

                <div className='actions__buttons'>
                    <button className='price__btn' disabled>
                        $ <strong> {price}</strong>
                    </button>

                    <div>
                        <button type='button' className='remove__btn' onClick={() => delBurger(id)}>
                            <FontAwesomeIcon icon={faShoppingCart}></FontAwesomeIcon> 
                            Remove
                        </button>
                    </div>
                </div>
            </div>

            { isToppinsOptions &&
            <div className='extra__section'>
                {(() => {
                    if (istoppinsType === burgerTopping) {
                        return (
                        <div>
                            <p className='title-section'>Toppings</p>
                            <div className='divider'></div>
                        
                        <form>
                            <div className='body__form'>
                                <label className="checkbox">Mayonnaise
                                    <input name="Mayonnaise" type="checkbox"/>
                                    <span className="check"></span>
                                </label>
                            
                                <label className="checkbox">Ketchup
                                    <input name="Ketchup" type="checkbox"/>
                                    <span className="check"></span>
                                </label>
                        
                                <label className="checkbox">Lettuce
                                    <input name="Lettuce" type="checkbox"/>
                                    <span className="check"></span>
                                </label>
                        
                                <label className="checkbox">Tomato
                                    <input name="Tomato" type="checkbox"/>
                                    <span className="check"></span>
                                </label>
                            </div>

                            <div className='body__form'>
                                <label className="checkbox">Pickles
                                    <input name="Pickles" type="checkbox"/>
                                    <span className="check"></span>
                                </label>
                        
                                <label className="checkbox">Onion
                                    <input name="Onion" type="checkbox"/>
                                    <span className="check"></span>
                                </label>
                            
                                <label className="checkbox">Bread
                                    <input name="Bread" type="checkbox"/>
                                    <span className="check"></span>
                                </label>
                            </div>
                        </form>
                    </div>
                    )
                    } else if (istoppinsType === combo) {
                    return (
                        <div>
                        <div className='container__section'>
                            <div className='size__section'>
                                <p className='title-section'>Size</p>
                                <div className='divider'></div>

                                <form>
                                    <div className='body__form'>
                                        <label className="radio">Small
                                            <input name="Small" type="radio" name="size" value="size" checked/>
                                            <span className="check-radio"></span>
                                        </label>
                                    </div>

                                    <div className='body__form'>
                                        <label className="radio">Big <span className='big-price'>(+2)</span>
                                            <input name="Big" type="radio" name="size" value="size"/>
                                            <span className="check-radio"></span>
                                        </label>
                                    </div>
                                </form>
                            </div>

                            <div className='flavor__section'>
                                <p className='title-section'>Flavor</p>
                                <div className='divider'></div>
                            
                                <form>
                                    <div className='body__form'>
                                        <label className="radio">Cola
                                            <input name="Cola" type="radio" name="flavor" value="coca"/>
                                            <span className="check-radio"></span>
                                        </label>
                                    
                                        <label className="radio">Lemon
                                            <input name="Lemon" type="radio" name="flavor" value="lemon"/>
                                            <span className="check-radio"></span>
                                        </label>
                                
                                        <label className="radio">Grapefruit
                                            <input name="Grapefruit" type="radio" name="flavor" value="grapefruit"/>
                                            <span className="check-radio"></span>
                                        </label>
                                
                                        <label className="radio">Orange
                                            <input name="Orange" type="radio" name="flavor" value="orange"/>
                                            <span className="check-radio"></span>
                                        </label>
                                    </div>

                                    <div className='body__form'>
                                        <label className="radio">Water
                                            <input name="Water" type="radio" name="flavor" value="water"/>
                                            <span className="check-radio"></span>
                                        </label>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div className='container__section'>
                            <div className='toppings__section'>
                                <p className='title-section'>Toppings</p>
                                <div className='divider'></div>
                                <form>
                                    <div className='body__form'>
                                        <label className="checkbox">Mayonnaise
                                            <input name="Mayonnaise" type="checkbox"/>
                                            <span className="check"></span>
                                        </label>
                                    
                                        <label className="checkbox">Ketchup
                                            <input name="Ketchup" type="checkbox"/>
                                            <span className="check"></span>
                                        </label>

                                        <label className="checkbox">Lettuce
                                            <input name="Lettuce" type="checkbox"/>
                                            <span className="check"></span>
                                        </label>

                                        <label className="checkbox">Tomato
                                            <input name="Tomato" type="checkbox"/>
                                            <span className="check"></span>
                                        </label>
                                    </div>

                                    <div className='body__form'>
                                        <label className="checkbox">Pickles
                                            <input name="Pickles" type="checkbox"/>
                                            <span className="check"></span>
                                        </label>

                                        <label className="checkbox">Onion
                                            <input name="Onion" type="checkbox"/>
                                            <span className="check"></span>
                                        </label>
                                    
                                        <label className="checkbox">Bread
                                            <input name="Bread" type="checkbox"/>
                                            <span className="check"></span>
                                        </label>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    )
                    } else if (istoppinsType === soda){
                    return (
                        <div className='container__section'>
                            <div className='size__section'>
                                <p className='title-section'>Size</p>
                                <div className='divider'></div>

                                <form>
                                    <div className='body__form'>
                                        <label className="radio">Small
                                            <input name="Small" type="radio" name="size" value="size" checked/>
                                            <span className="check-radio"></span>
                                        </label>
                                    </div>

                                    <div className='body__form'>
                                        <label className="radio">Big <span className='big-price'>(+0.7)</span>
                                            <input name="Big" type="radio" name="size" value="size"/>
                                            <span className="check-radio"></span>
                                        </label>
                                    </div>
                                </form>
                            </div>

                            <div className='flavor__section'>
                                <p className='title-section'>Flavor</p>
                                <div className='divider'></div>
                            
                                <form>
                                    <div className='body__form'>
                                        <label className="radio">Cola
                                            <input name="Cola" type="radio" name="flavor" value="coca"/>
                                            <span className="check-radio"></span>
                                        </label>
                                    
                                        <label className="radio">Lemon
                                            <input name="Lemon" type="radio" name="flavor" value="lemon"/>
                                            <span className="check-radio"></span>
                                        </label>
                                
                                        <label className="radio">Grapefruit
                                            <input name="Grapefruit" type="radio" name="flavor" value="grapefruit"/>
                                            <span className="check-radio"></span>
                                        </label>
                                
                                        <label className="radio">Orange
                                            <input name="Orange" type="radio" name="flavor" value="orange"/>
                                            <span className="check-radio"></span>
                                        </label>
                                    </div>

                                    <div className='body__form'>
                                        <label className="radio">Water
                                            <input name="Water" type="radio" name="flavor" value="water"/>
                                            <span className="check-radio"></span>
                                        </label>
                                    </div>
                                </form>
                            </div>
                        </div>
                    )
                    } else {
                        return (
                            <div>
                                <div>
                                    <p className='title-section'>Size</p>
                                    <div className='divider'></div>
    
                                    <form>
                                        <div className='body__form'>
                                            <label className="radio">Small
                                                <input name="Small" type="radio" name="size" value="size" checked/>
                                                <span className="check-radio"></span>
                                            </label>
                                        </div>

                                        <div className='body__form'>
                                            <label className="radio">Big <span className='big-price'>(+2.26)</span> 
                                                <input name="Big" type="radio" name="size" value="size"/>
                                                <span className="check-radio"></span>
                                            </label>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        ) 
                    }
                    })()}
            </div>}
        </div>
    );
};

export default Burger;