import React, { Fragment, useState } from 'react';
import Burger from "./components/Burger";
import Combo from "./components/Combo";
import Cart from "./components/Cart";
import Other from "./components/Other";
import "./styles/styles.css";

// Toats
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

// IMPORT IMGS
import BurgerNormal from "./images/burgerNormal.svg"
import BurgerDouble from "./images/burgerDouble.svg"
import BurgerCheese from "./images/burgerCheese.svg"
import BurgerCheeseDouble from "./images/burgerCheeseDouble.svg" 
import Fries from "./images/fries.svg" 
import Soda from "./images/soda.svg" 
import Logo from "./images/logo.svg"

function App() {
    const [combos, setCombo] = useState([
        { id: 1, img: BurgerNormal, name: 'Combo Burger', details: 'Simple but jucy burger, Choose your toppings.', price: 9.99, toppins: true, toppingType: 'combo' },
        { id: 2, img: BurgerDouble, name: 'Combo Double Burger', details: 'Double jucy delicious burger. Choose your toppings.', price: 10.99, toppins: true, toppingType: 'combo' },
        { id: 3, img: BurgerCheese, name: 'Combo Burger + Cheese', details: 'What´s a buger without cheese? Choose your toppings.', price: 11.99, toppins: true, toppingType: 'combo' },
        { id: 4, img: BurgerCheeseDouble, name: 'Combo Double Burger + Cheese', details: 'What’s a burger without melted delicious DOUBLE cheese?', price: 15.49, toppins: true, toppingType: 'combo' }
    ])

    const [burgers, setBurgers] = useState([
        { id: 5, img: BurgerNormal, name: 'Burger', details: 'simple but juicy burger. Choose your toppings.', price: 6.25, toppins: true, toppingType: 'burger' },
        { id: 6, img: BurgerDouble, name: 'Double Burger', details: 'Double juicy delicious burger. Choose your toppings.', price: 8.49, toppins: true, toppingType: 'burger' },
        { id: 7, img: BurgerCheese, name: 'Burger + Cheese', details: 'What’s a burger without cheese? Choose your toppings.', price: 9.00, toppins: true, toppingType: 'burger' },
        { id: 8, img: BurgerCheeseDouble, name: 'Double Burger + Cheese', details: 'What’s a burger without melted delicious DOUBLE cheese?', price: 13.00, toppins: true, toppingType: 'burger' }   
    ])

    const [others, setOthers] = useState([
        { id: 9, img: Fries, name: 'Fries', details: 'Crunchy unique fries.', price: 3.49, toppins: true, toppingType: 'fries' },
        { id: 10, img: Soda, name: 'Soda', details: 'Accompany your burger with a soda.', price: 1.00, toppins: true, toppingType: 'soda' } 
    ])

    const [cart, setCart] = useState([]);

    return ( 
        <Fragment >
            <section className='container'>
                <ToastContainer></ToastContainer>
                <section className='container__header'>
                    
                    <div className='container__header__menu'>
                        <div className='logo'>
                            <img src={Logo} alt='logo for simplerburger'/>
                        </div>

                        
                        <Cart cart = {cart} setCart = {setCart}/>
                    </div>

                    {/* header */}
                    <div className='container__header__banner'>
                        {/* Img banckground */}
                    </div>
                </section>

                <section className='container__combo'>
                    <h3>COMPLETE FOR YOU</h3>
                    <p className="comercial__title">COMBO</p>
                    {combos.map((combo) => (
                            <Combo 
                                key = {combo.id} 
                                combo = {combo}
                                cart = {cart}
                                setCart = {setCart}
                                combos = {combos}
                            />    
                    ))}
                </section>

                <section className='container__burguers'>
                    <h3>DELICIOUS</h3>
                    <p className="comercial__title">Burgers</p>
                        {burgers.map((burger) => (
                            <Burger 
                                key = {burger.id} 
                                burger = {burger}
                                cart = {cart}
                                setCart = {setCart}
                                burgers = {burgers}
                            />    
                        ))}
                </section>

                <section className='container__other'>
                    <h3>MORE?</h3>
                    <p className="comercial__title">Other</p>
                        {others.map((other) => (
                            <Other 
                                key = {other.id} 
                                other = {other}
                                cart = {cart}
                                setCart = {setCart}
                                others = {others}
                            />    
                        ))}
                </section>
            </section>

            <footer>
                <div className='logo'>
                    <img src={Logo} alt='logo for simplerburger'/>
                </div>
            </footer>
        </Fragment>
    );
}

export default App;